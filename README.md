# NMax

Simple gem, which count numbers from STDIN.

Restrictions:
 - Max length of number - 1000 digits

## Installation

Add this line to your application's Gemfile:

```
gem 'n_max'
```

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install n_max

## Usage

    $ cat filename | n_max count
