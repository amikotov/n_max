# frozen_string_literal: true

RSpec.describe NMax::FindNumbers do
  subject(:numbers) { described_class.call(stdin, count: count) }

  let(:stdin) { StringIO.new(File.read('spec/fixtures/small.txt')) }
  let(:count) { 5 }

  it 'gets correct numbers' do
    expect(numbers).to eq([1515, 15, 13])
  end

  context 'when stdin is empty' do
    let(:stdin) { StringIO.new('') }

    it 'does not return anything' do
      expect(numbers).to be_nil
    end
  end

  context 'when stdin is bigger' do
    let(:stdin) { StringIO.new(File.read('spec/fixtures/big.txt')) }

    it 'gets correct count of numbers' do
      expect(numbers.count).to eq(count)
    end
  end
end
