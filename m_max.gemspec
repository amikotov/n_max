lib = File.expand_path('lib', __dir__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'n_max/version'

Gem::Specification.new do |spec|
  spec.name          = 'n_max'
  spec.version       = NMax::VERSION
  spec.authors       = ['Alexander Kotov']
  spec.email         = ['amikotov@gmail.com']
  spec.summary       = 'Small tool for count numbers from STDIN'
  spec.homepage      = 'https://bitbucket.org/amikotov/n_max'
  spec.license       = 'MIT'

  spec.required_ruby_version = '>= 2.3'
  spec.files         = `git ls-files -z`.split("\x0").reject { |f| f.match(%r{^(test|spec|features)/}) }
  spec.bindir        = 'bin'
  spec.executables   << 'n_max'
  spec.require_paths = ['lib']

  spec.add_development_dependency 'bundler', '~> 1.12'
  spec.add_development_dependency 'pry'
  spec.add_development_dependency 'rake', '~> 10.0'
  spec.add_development_dependency 'rspec', '~> 3.0'
end
