# frozen_string_literal: true

module NMax
  class FindNumbers
    attr_reader :stdin, :count, :batch_size, :numbers_array

    def self.call(*args)
      new(*args).find
    end

    def initialize(stdin, count:, batch_size: 10_000)
      @stdin = stdin
      @count = count
      @batch_size = batch_size
      @numbers_array = []
    end

    def find
      batch = stdin.read(batch_size)

      until batch.nil?
        batch_numbers = batch.scan(/\d{1,1000}/).map(&:to_i)
        numbers_array.concat(batch_numbers)
        batch = stdin.read(batch_size)
      end

      obtain_numbers
    end

    private

    def obtain_numbers
      return if numbers_array.empty?

      numbers_array.uniq!.sort!.reverse!.first(count)
    end
  end
end
